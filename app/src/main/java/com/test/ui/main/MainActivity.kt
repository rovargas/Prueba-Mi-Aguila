package com.test.ui.main

import android.location.Location
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.test.R
import com.test.databinding.ActivityMainBinding
import com.test.util.BaseViewModelFactory
import com.test.util.GpsTracker

class MainActivity : AppCompatActivity(), OnMapReadyCallback {

    //region variables
    private lateinit var binding: ActivityMainBinding
    private lateinit var mapsViewModel: MapsViewModel
    private var mMap: GoogleMap? = null
    private var isAnimation = false
    private var handler = Handler()
    //endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        mapsViewModel =
            MapsViewModel.create(this, BaseViewModelFactory { MapsViewModel(GpsTracker(this)) })
        binding.model = mapsViewModel
        init()
        initObserver()
    }

    private fun init() {
        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
    }

    private fun initObserver() {
        mapsViewModel.locationLiveData.observe(this, Observer {
            mMap?.run {
                this.addMarker(
                    MarkerOptions().position(LatLng(it.latitude, it.longitude))
                )
            }
            animationCameraMap(it)
        })
    }

    private fun animationCameraMap(location: Location) {
        if (!isAnimation) {
            mMap?.let {
                val newCamPos = CameraPosition(
                    LatLng(location.latitude, location.longitude),
                    16.5f,
                    it.cameraPosition.tilt,
                    it.cameraPosition.bearing
                )
                it.animateCamera(CameraUpdateFactory.newCameraPosition(newCamPos), 2000, null)
            }
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0
        p0?.let {
            it.isMyLocationEnabled = true
            it.uiSettings.isCompassEnabled = true
            it.uiSettings.isIndoorLevelPickerEnabled = false
            it.uiSettings.isTiltGesturesEnabled = false
            it.uiSettings.isRotateGesturesEnabled = false
            it.uiSettings.isMyLocationButtonEnabled = true
            it.setOnCameraIdleListener {
                handler.postDelayed({
                    isAnimation = false
                }, 3000)
            }
            it.setOnCameraMoveStartedListener {
                handler.removeCallbacksAndMessages(null)
                isAnimation = true
            }
        }
    }

}
