package com.test.ui.main

import android.location.Location
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProviders
import com.test.util.BaseViewModelFactory
import com.test.util.GpsTracker
import java.text.DecimalFormat


class MapsViewModel(private val gpsTracker: GpsTracker) : ViewModel() {

    //region variables
    val locationLiveData: LiveData<Location>
        get() = gpsTracker.liveData
    val speedLiveData: LiveData<String> = Transformations.map(locationLiveData) {
        if (it != null) {
            if (it.hasSpeed()) {
                val df = DecimalFormat("#.#")
                "${df.format(it.speed * 3.6)}\nkm/h"
            } else {
                "0\nkm/h"
            }
        } else {
            "0\nkm/h"
        }
    }

    override fun onCleared() {
        super.onCleared()
        gpsTracker.stopUsingGPS()
    }

    companion object {
        fun create(
            activity: FragmentActivity,
            viewModelFactory: BaseViewModelFactory<MapsViewModel>
        ): MapsViewModel {
            return ViewModelProviders.of(activity, viewModelFactory).get(MapsViewModel::class.java)
        }
    }
    //endregion
}