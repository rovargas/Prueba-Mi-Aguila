package com.test.ui.base

import android.app.Application


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        @get:Synchronized
        lateinit var instance: App
            private set
    }

}