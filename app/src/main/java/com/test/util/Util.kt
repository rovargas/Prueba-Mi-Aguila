package com.test.util

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager

object Util {
    fun hasPermissions(context: Context): Boolean {
        for (permission in PERMISSIONS)
            if (context.checkCallingOrSelfPermission(permission) != PackageManager.PERMISSION_GRANTED)
                return false

        return true
    }

    val PERMISSIONS = arrayOf(
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.ACCESS_FINE_LOCATION
    )
}



